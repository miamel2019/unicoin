/**
 * Generate token for specific user
 * @param {deakin.edu.au.GenerateToken} options
 * @transaction
 */
async function generateToken(options) {
    try {
        let factory = await getFactory();

        let assetRegistry = await getAssetRegistry('deakin.edu.au.Token');
        let walletRegistry = await getAssetRegistry('deakin.edu.au.Wallet');

        let tokenID = options.user.getIdentifier()+'_'+options.bank.getIdentifier()+'_token_'+randomString(10);
        console.log(tokenID);
        let token = await factory.newResource('deakin.edu.au', 'Token', tokenID);

        token.user = options.user;
        token.bank = options.bank;
        token.value = options.value;

        var transferTransaction = factory.newConcept('deakin.edu.au', 'TokenTransaction');
        transferTransaction.type = "DEPOSIT"
        transferTransaction.owner = token.user;

        token.status = "AVAILABLE";
        token.transactions = [transferTransaction];

        let result = await query('getUserWallet', { user: 'resource:deakin.edu.au.User#'+options.user.getIdentifier() });
        
        if (typeof result[0] != 'undefined') {
            let wallet = result[0];

            wallet.value += token.value;
            wallet.realValue += token.value;

            await assetRegistry.add(token);
            await walletRegistry.update(wallet);
        } else {
            throw new Error("Cannot find user's wallet.");
        }

    } catch(exception) {
        console.error(exception);
    }
}

function randomString(length) {
    let chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = '';
    for (let i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];

    return result;
}


/**
 * Transfer token
 * @param {deakin.edu.au.TransferToken} options
 * @transaction
 */
async function transfer(options) {
    let factory = await getFactory();

    let tokenRegistry = await getAssetRegistry('deakin.edu.au.Token');
    let walletRegistry = await getAssetRegistry('deakin.edu.au.Wallet');
    let userRegistry = await getParticipantRegistry('deakin.edu.au.User');

    let token = options.token;

    if (token.user.getIdentifier() == options.owner.getIdentifier()) {
        
        let ownerWallet = await query('getUserWallet', { user: 'resource:deakin.edu.au.User#'+options.owner.getIdentifier() });

        if (typeof ownerWallet[0] != 'undefined') {
            ownerWallet = ownerWallet[0];

            if (ownerWallet.realValue == 0) {
                throw new Error('User\'s wallet seems to be empty.');
            }

            if (token.status == 'AVAILABLE') {
                ownerWallet.value -= token.value;
                await walletRegistry.update(ownerWallet);

                token.status = 'PROCESSING';
                await tokenRegistry.update(token);

                let payeeWallet = await query('getUserWallet', { user: 'resource:deakin.edu.au.User#'+options.payee.getIdentifier() });

                if (typeof payeeWallet[0] != 'undefined') {
                    payeeWallet = payeeWallet[0];

                    payeeWallet.value += token.value;
                    await walletRegistry.update(payeeWallet);

                    payeeWallet.realValue += token.value;

                    let transferTransaction = factory.newConcept('deakin.edu.au', 'TokenTransaction');
                    transferTransaction.type = "TRANSFER"
                    transferTransaction.owner = options.owner;
                    transferTransaction.payee = options.payee;

                    token.transactions.push(transferTransaction);
                    token.user = options.payee;
                    token.status = 'AVAILABLE';

                    await tokenRegistry.update(token);
                    await walletRegistry.update(payeeWallet);

                    ownerWallet.realValue -= token.value;
                    await walletRegistry.update(ownerWallet);
                } else {
                    ownerWallet.value += token.value;
                    await walletRegistry.update(ownerWallet);

                    token.status = 'AVAILABLE';
                    await tokenRegistry.update(token);

                    throw new Error ("Payee's wallet doesnot exist");
                }

            } else {
                throw new Error('This token cannot be processed.'); 
            }
        } else {
            throw new Error('User\'s wallet doesnot exist.');
        }
    } else {
        throw new Error('This token does not belong to this user.');
    }
}

/**
 * Adds new user
 * @param {deakin.edu.au.AddUser} userDetails
 * @transaction
 */
function addUser (userDetails) {
    return getParticipantRegistry('deakin.edu.au.User')
    .then((participantRegistry) => {
        let user = getFactory().newResource('deakin.edu.au', 'User', userDetails.id);

        user.admin = userDetails.admin;
        user.banker = userDetails.banker;
        user.name = userDetails.name;

        return participantRegistry.add(user).then(() => {
            return getAssetRegistry('deakin.edu.au.Wallet')
            .then((walletRegistry) => {
                let wallet = getFactory().newResource('deakin.edu.au', 'Wallet', 'wallet'+user.userId);

                wallet.value = 0;
                wallet.realValue = 0;
                wallet.user = getFactory().newRelationship('deakin.edu.au', 'User', user.userId);

                return walletRegistry.add(wallet);
            });
        });
    })
    .catch(function (error) {
        throw new Error(error);
    });
}

/**
 * Adds new Banker
 * @param {deakin.edu.au.AddBanker} userDetails
 * @transaction
 */
function addBanker (userDetails) {
    return getParticipantRegistry('deakin.edu.au.Banker')
    .then((participantRegistry) => {
        let user = getFactory().newResource('deakin.edu.au', 'Banker', userDetails.id);

        user.name = userDetails.name;
        user.phone = userDetails.phone;
        user.email = userDetails.email;
        user.admin = userDetails.admin;

        return participantRegistry.add(user);
    })
    .catch(function (error) {
        throw new Error(error);
    });
}

/**
 * Adds new Administrator
 * @param {deakin.edu.au.AddAdmin} userDetails
 * @transaction
 */
function addAdministrator (userDetails) {
    return getParticipantRegistry('deakin.edu.au.Administrator')
    .then((participantRegistry) => {
        let user = getFactory().newResource('deakin.edu.au', 'Administrator', userDetails.id);

        user.name = userDetails.name;

        return participantRegistry.add(user);
    })
    .catch(function (error) {
        throw new Error(error);
    });
}
